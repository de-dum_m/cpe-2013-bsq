##
## Makefile for my_ls in /home/de-dum_m/code/B1-Systeme_Unix/my_ls
## 
## Made by de-dum_m
## Login   <de-dum_m@epitech.net>
## 
## Started on  Thu Nov 21 12:18:32 2013 de-dum_m
## Last update Sun Dec  8 10:43:08 2013 de-dum_m
##

NAME	= bsq

SRC	= src/main.c \
	src/get_next_line.c \
	src/bsq_finder.c \
	src/bsq_errors.c

SRO	= $(SRC:.c=.o)

LIBDIR	= my_lib/

MLIB	= my_lib/my

SRCLIB	= my_lib/libmy.a

SRCHEAD	= my_lib/my.h

$(NAME): $(SRO)
	@make -C $(MLIB) -f Makefile
	cc -o $(NAME) $(SRO) -I$(LIBDIR) -L$(LIBDIR) -lmy

all:	$(NAME)

clean:
	@rm -f $(SRO)
	@make clean -C $(MLIB) -f Makefile

fclean: clean
	rm -f $(NAME)
	@make fclean -C $(MLIB) -f Makefile

re:	fclean all

.PHONY: all clean fclean re
