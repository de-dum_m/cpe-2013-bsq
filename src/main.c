/*
** main.c for BSQ in /home/de-dum_m/code/B1-C-Prog_Elem/BSQ
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Dec  6 14:40:28 2013 de-dum_m
** Last update Sun Dec  8 10:43:36 2013 de-dum_m
*/

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../my_lib/my/my.h"
#include "bsq.h"

int	print_xxx(t_bsq *big, int j)
{
  int	xs;

  xs = 0;
  while (xs < big->size)
    {
      my_putchar('x');
      xs = xs + 1;
    }
  return (j + big->size - 1);
}

void	print_da_grid(char **grid, t_bsq *big)
{
  int	i;
  int	j;

  i = 0;
  while (grid[i])
    {
      j = 0;
      while (grid[i][j])
	{
	  if (j == big->y && i - big->x < big->size && i >= big->x)
	    j = print_xxx(big, j);
	  else
	    my_putchar(grid[i][j]);
	  j = j + 1;
	}
      my_putchar('\n');
      i = i + 1;
    }
}

int	check_grid(char **grid)
{
  int	i;
  int	j;

  i = 0;
  while (grid[i])
    {
      j = 0;
      while (grid[i][j])
	{
	  if (grid[i][j] != '.' && grid[i][j] != 'o')
	    return (0);
	  j = j + 1;
	}
      i = i + 1;
    }
  return (1);
}

char	**make_grid(int fd)
{
  int	i;
  char	**grid;
  int	g_lines;

  i = 0;
  if ((g_lines = my_getnbr(get_next_line(fd))) < 0)
    return (my_usage(BAD_MAP));
  if ((grid = malloc((sizeof(char *)) * (g_lines + 2))) == NULL)
    return (my_usage(MALLOC_FAILED));
  while (i < g_lines)
    {
      if ((grid[i] = get_next_line(fd)) == NULL)
	return (my_usage(GRID_TOO_SHORT));
      i = i + 1;
    }
  grid[i] = NULL;
  if (get_next_line(fd) != NULL)
    return (my_usage(TOO_MANY_LINES));
  if ((check_grid(grid)) == 0)
    return (my_usage(BAD_MAP));
  return (grid);
}

int	main(int ac, char **av)
{
  int	fd;
  char	**grid;
  t_bsq	big;

  big.x = 0;
  big.y = 0;
  big.size = 0;
  if (ac == 2)
    {
      if ((fd = open(av[1], O_RDWR)) == -1)
	return (my_usage(OPEN_FAIL));
      if ((grid = make_grid(fd)) == NULL)
	return (0);
      find_da_square(grid, &big);
      print_da_grid(grid, &big);
    }
  else
    return (my_usage(ARGS));
  return (0);
}
