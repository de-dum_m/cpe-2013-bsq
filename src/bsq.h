/*
** bsq.h for BSQ in /home/de-dum_m/code/B1-C-Prog_Elem/BSQ
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Dec  6 14:42:29 2013 de-dum_m
** Last update Sun Dec  8 10:23:12 2013 de-dum_m
*/

#ifndef BSQ_H_
#define BSQ_H_

# define ARGS		0
# define OPEN_FAIL	1
# define BAD_MAP	2
# define TOO_MANY_LINES	3
# define MALLOC_FAILED	4
# define GRID_TOO_SHORT 5

typedef struct	s_bsq
{
  int	x;
  int	y;
  int	size;
}		t_bsq;

void	*my_usage(int errornb);
char	*get_next_line(int fd);
int	get_square(char **grid, int i, int j, t_bsq *big);
void	find_da_square(char **grid, t_bsq *big);

#endif /* BSQ_H_ */
