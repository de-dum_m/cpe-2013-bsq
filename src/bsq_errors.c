/*
** bsq_errors.c for BSQ in /home/de-dum_m/code/B1-C-Prog_Elem/BSQ
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Dec  7 18:15:54 2013 de-dum_m
** Last update Sat Dec  7 19:14:33 2013 de-dum_m
*/

#include <stdlib.h>

void	*my_usage(int errornb)
{
  if (errornb == 0)
    my_printf("Error: insufficient or too many args.\n");
  else if (errornb == 1)
    my_printf("Error: open failed, please give a correct map.\n");
  else if (errornb == 2)
    my_printf("Error: not a grid, please give a correct map\n");
  else if (errornb == 3)
    my_printf("Error: too many lines.\n");
  else if (errornb == 4)
    my_printf("Error: malloc failed for some reason.\n");
  else if (errornb == 5)
    my_printf("Error: insufficient number of lines.\n");
  my_printf("Usage: ./bsq grid\n");
  return (NULL);
}
