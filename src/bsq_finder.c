/*
** bsq_finder.c for BSQ in /home/de-dum_m/code/B1-C-Prog_Elem/BSQ
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Dec  7 17:37:02 2013 de-dum_m
** Last update Sat Dec  7 19:20:56 2013 de-dum_m
*/

#include "bsq.h"

int	square_max(char **grid, int x, int y, t_bsq *big)
{
  int	max;

  max = 0;
  while (grid[x][y] != 'o' && grid[x][y])
    {
      y = y + 1;
      max = max + 1;
    }
  return (max);
}

int	get_square(char **grid, int i, int j, t_bsq *big)
{
  int	x;
  int	y;
  int	size_line;
  int	smallest_line;

  y = j;
  smallest_line = square_max(grid, i, j, big);
  while (y - j < smallest_line)
    {
      x = i;
      size_line = 0;
      if (smallest_line <= big->size)
	return (0);
      while (grid[x] && grid[x][y] != 'o' && size_line <= smallest_line)
	{
	  x = x + 1;
	  size_line = size_line + 1;
	}
      if (size_line < smallest_line && y - j > size_line)
	return (y - j);
      else if (size_line - 1< smallest_line)
	smallest_line = size_line;
      y = y + 1;
    }
  return (smallest_line);
}

void	find_da_square(char **grid, t_bsq *big)
{
  int	i;
  int	j;
  int	size;

  i = 0;
  big->size = 0;
  while (grid[i])
    {
      j = 0;
      while (grid[i][j])
	{
	  if (grid[i][j] == '.')
	    if ((size = get_square(grid, i, j, big)) > big->size)
	      {
		big->x = i;
		big->y = j;
		big->size = size;
	      }
	  j = j + 1;
	}
      i = i + 1;
    }
}
