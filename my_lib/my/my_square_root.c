/*
** my_square_root.c for my_square_root in /home/de-dum_m/code/my_square_root
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Oct 20 14:59:36 2013 de-dum_m
** Last update Sun Oct 20 15:15:26 2013 de-dum_m
*/

int	my_square_root(int number)
{
  int	i;

  i = 0;
  while (i * i < number)
    i = i + 1;
  if (i * i == number)
    return (i);
  else
    return (0);
}
