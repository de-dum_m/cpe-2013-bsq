/*
** my_isneg.c for my_isneg in /home/de-dum_m/rendu/Piscine-C-Jour_03
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Wed Oct  2 11:22:12 2013 de-dum_m
** Last update Sun Oct 20 18:24:34 2013 de-dum_m
*/

int	my_isneg(int nb)
{
  if (nb >= 0)
    my_putchar("P");
  else
    my_putchar("N");
  return (0);
}
