/*
** my_printf.c for my_printf in /home/de-dum_m/code/B1-Systeme_Unix/TP
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Nov 11 16:01:31 2013 de-dum_m
** Last update Sun Nov 17 09:42:12 2013 de-dum_m
*/

#include <stdarg.h>
#include <stdio.h>
#include "my_printf.h"

void	my_put_hex(unsigned int ix, char type)
{
  char	*base;

  base = "0123456789abcdef";
  if (type == 'X')
    base = "0123456789ABCDEF";
  my_count_putnbr_base(ix, base);
}

void	my_putstr_weird(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 32 && str[i] < 127)
	my_count_putchar(str[i]);
      else
	{
	  my_count_putchar('\\');
	  if (str[i] <= 7)
	    my_count_putchar('0');
	  if (str[i] <= 63)
	    my_count_putchar('0');
	  my_count_putnbr_base(str[i], "01234567");
	}
      i = i + 1;
    }
}

void	my_put_ptr(unsigned int ptr)
{
  if (ptr > 0)
    my_count_putstr("0x");
  if (ptr > 16777215)
    my_count_putstr("7fff");
  if (ptr == 0)
    my_count_putstr("(nil)");
  else if (ptr < 16777215 && ptr > 16777215)
    my_count_putstr("00");
  else if (ptr < 268435455 && ptr > 16777215)
    my_count_putchar('0');
  if (ptr > 0)
    my_count_putnbr_base(ptr, "0123456789abcdef");
}

int   	det_type(va_list ap, char type)
{
  if (type == 'd' || type == 'i')
    my_count_put_nbr(va_arg(ap, int));
  else if (type == 's')
    my_count_putstr(va_arg(ap, char*));
  else if (type == 'p')
    my_put_ptr(va_arg(ap, unsigned int));
  else if (type == 'u')
    my_put_unsigned_nbr(va_arg(ap, unsigned int));
  else if (type == 'c' || type == 'C')
    my_count_putchar(va_arg(ap, int));
  else if (type == 'x' || type == 'X')
    my_put_hex(va_arg(ap, unsigned int), type);
  else if (type == 'b')
    my_count_putnbr_base(va_arg(ap, unsigned int), "01");
  else if (type == 'S')
    my_putstr_weird(va_arg(ap, char*));
  else if (type == 'o')
    my_count_putnbr_base(va_arg(ap, unsigned int), "01234567");
  else
    return (0);
  return (1);
}

int		my_printf(const char *str, ...)
{
  va_list	ap;
  int		i;

  i = 0;
  va_start(ap, str);
  while (str[i])
    {
      if (str[i] == '%' && str[i + 1] != '%')
	{
	  if (det_type(ap, str[i + 1]) == 0)
	    return (0);
	  i = i + 1;
	}
      else if (str[i] == '%')
	{
	  my_count_putchar('%');
	  i = i + 1;
	}
      else
	my_count_putchar(str[i]);
      i = i + 1;
    }
  va_end(ap);
  return (count_prints() - 1);
}
