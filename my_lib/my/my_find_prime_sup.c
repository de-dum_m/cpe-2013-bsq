/*
** my_find_prime_sup.c for my_find_prime_sup in /home/de-dum_m/code/my_is_prime
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Oct 20 20:24:31 2013 de-dum_m
** Last update Sun Oct 20 20:28:57 2013 de-dum_m
*/

int	my_find_prime_sup(int nb)
{
  while (my_is_prime(nb) == 0)
    nb = nb + 1;
  return (nb);
}
