/*
** my_is_prime.c for my_is_prime in /home/de-dum_m/code
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Oct 20 15:18:40 2013 de-dum_m
** Last update Sun Oct 20 19:47:33 2013 de-dum_m
*/

int	my_is_prime(int	nb)
{
  int	i;
  int	j;

  i = 0;
  j = 2;
  while (i * i <= nb && i <= i * i)
    i = i + 1;
  while (j <= i)
    {
      if (nb % j == 0)
	return (0);
      j = j + 1;
    }
  return (1);
}
