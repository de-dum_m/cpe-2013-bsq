/*
** my_strcat.c for my_strcat.c in /home/de-dum_m/rendu/Piscine-C-Jour_07/ex_01
** 
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
** 
** Started on  Tue Oct  8 15:36:05 2013 de-dum_m
** Last update Tue Oct  8 18:33:25 2013 de-dum_m
*/

int	my_strlcat(char *dest, char *src, int nb)
{
  int	i;
  int	j;

  while (dest[i] != '\0')
    i = i + 1;
  while (j <= nb && src[j] != '\0')
    {
      dest[i + j] = src[i];
      j = j + 1;
    }
  return (i + j);
}
